## Digitales Wartezimmer - PDF Handler Service

### Requirements

- PHP 7.4
- PDFTK

### Installation

- install `openjdk`
- add `composer.phar` from `https://getcomposer.org/download/` to `htdocs/`
- `cd htdocs/`
- run `php74 composer.phar install`
- if on apache, add .htaccess file to `htdocs/public/` (on nginx, another configuration is required)

### Sources

- Questionnaire: https://www.berlin.de/ba-mitte/politik-und-verwaltung/aemter/gesundheitsamt/corona/

### How to find field names of PDF

- use Adobe Acrobat DC
- use tool "create form"

### How to optimize PDF

- use Adobe Acrobat DC
- rename second field "Datum" to "Kontaktdatum" (because there's another field called "Datum"; in all other languages than de-DE it's already done)
- remove submit field, remove second page (not needed because we not already created a full version)