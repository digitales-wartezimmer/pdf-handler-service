<?php

namespace Application;

use Application\Flight\Engine;
use Application\Route\Create;
use flight;

class App{

  public function __construct(){
    $engine = $this->setupEngine();
    $this->setupRoutes($engine);
    $this->startEngine($engine);
  }

  private function setupEngine(){
    return new Engine();
  }

  private function setupRoutes(flight\Engine $engine){
    $engine->map('error', function(\Exception $e) use ($engine){
      $engine->json([
        'errorCode' => 'UNDEFINED',
        'message' => $e->getMessage()
      ]);
    });

    $engine->route('POST /create', function() use ($engine){
      $route = new Create($engine);
      $route->execute();
    });

    $engine->route('GET /health/ready', function() use ($engine){
      $engine->json([
        'success' => true
      ], 200);
    });

    $engine->route('GET /health/live', function() use ($engine){
      $engine->json([
        'success' => true
      ], 200);
    });
  }

  private function startEngine(flight\Engine $engine){
    $engine->start();
  }

}