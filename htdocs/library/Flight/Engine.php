<?php

namespace Application\Flight;

class Engine extends \flight\Engine {

  public function _json($data, $code = 200, $encode = true, $charset = 'utf-8', $option = 0){
    parent::_json($data, $code, $encode, $charset, $option);

    die();
  }

}