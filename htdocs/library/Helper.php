<?php

namespace Application;

class Helper{

  public static function getInputJsonParams(){
    return json_decode(file_get_contents('php://input'), true);
  }

  public static function getInputJsonParam($keyCheck){
    $params = self::getInputJsonParams();

    if (!is_array($params)){
      return null;
    }

    foreach ($params as $key => $value){
      if ($keyCheck !== $key){
        continue;
      }

      return $value;
    }

    return null;
  }

}