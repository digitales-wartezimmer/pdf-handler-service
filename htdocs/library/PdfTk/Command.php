<?php

namespace Application\PdfTk;

/**
 * necessary change of library to allow umlauts
 * https://github.com/mikehaertl/php-pdftk/issues/4
 */
class Command extends \mikehaertl\pdftk\Command {

  protected $_command = 'java -jar '.__DIR__.'/../../../libraries/mcpdf-0.2.4-jar-with-dependencies.jar';

  /**
   * Process input PDF files and create respective command arguments
   */
  protected function processInputFiles(){
    $passwords = array();
    foreach ($this->_files as $handle => $file) {
      $this->addArg('[REMOVE]', $file['name']);

      if ($file['password'] !== null) {
        $passwords[$handle] = $file['password'];
      }
    }
    if ($passwords !== array()) {
      $this->addArg('input_pw');
      foreach ($passwords as $handle => $password) {
        $this->addArg($handle . '=', $password);
      }
    }
  }

  /**
   * Process options and create respective command arguments
   * @param string|null $filename if provided an 'output' option will be
   * added
   */
  protected function processOptions($filename = null)
  {
    // output must be first option after operation
    if ($filename !== null) {
      $this->addArg(' > ',$filename, false);
    }
    foreach ($this->_options as $option) {
      if (is_array($option)) {
        $this->addArg($option[0], $option[1], $option[2]);
      } else {
        $this->addArg($option);
      }
    }
  }

  /**
   * @return string the command args that where set with setArgs() or added
   * with addArg() separated by spaces
   */
  public function getArgs(){
    $argsOptimized = [];

    foreach ($this->_args as $arg){
      $argOptimized = str_replace("'[REMOVE]'", '', $arg);
      $argsOptimized[] = $argOptimized;
    }

    $response = implode(' ', $argsOptimized);

    $response = str_replace("'fill_form'", "'fill_form' - output - flatten <", $response);

    return $response;
  }

}