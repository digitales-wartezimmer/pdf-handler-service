<?php

namespace Application\PdfTk;

/**
 * necessary change of library to allow umlauts
 * https://github.com/mikehaertl/php-pdftk/issues/4
*/
class Pdf extends \mikehaertl\pdftk\Pdf {

  /**
   * @return Command the command instance that executes pdftk
   */
  public function getCommand()
  {
    if ($this->_command === null) {
      $this->_command = new Command;
    }
    return $this->_command;
  }

}