<?php

namespace Application;

use flight;

abstract class Route{

  private $engine;

  public function __construct(flight\Engine $engine){
    $this->engine = $engine;
  }

  protected function getEngine(){
    return $this->engine;
  }

  public abstract function execute();

}