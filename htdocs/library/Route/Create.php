<?php

namespace Application\Route;

use Application;
use Valitron;

class Create extends Application\Route {

  const STRING_INVALID = '[NOT_VALID]';

  public function execute(){
    $dataJson = $this->getDataJson();
    $this->validateData($dataJson);

    $this->createPdf($dataJson);
  }

  private function getDataJson(){
    $dataJson = Application\Helper::getInputJsonParams();

    // special rule because of bug: https://github.com/vlucas/valitron/issues/251
    if (!isset($dataJson['symptoms']) || ctype_space($dataJson['symptoms']) || empty($dataJson['symptoms'])){
      $dataJson['symptoms'] = static::STRING_INVALID;
    }

    return $dataJson;
  }

  private function validateData(array $dataJson){
    $v = new Valitron\Validator($dataJson);

    $v->rule('required', 'firstName');

    $v->rule('required', 'lastName');

    $v->rule('required', 'birthday');

    $dateTime = new \DateTime();
    $dateTime->modify('+ 1 day');
    $dateTomorrow = $dateTime->format('Y-m-d');

    $v->rule('dateBefore', 'birthday', $dateTomorrow);

    $v->rule('required', 'gender');
    $v->rule('in', 'gender', ['male', 'female', 'diverse']);

    $v->rule('required', 'address.street');

    $v->rule('required', 'address.zipcode');

    $v->rule('required', 'address.city');

    $v->rule('required', 'phone');

    $v->rule('optional', 'email');
    $v->rule('email', 'email');

    $v->rule('required', 'sourceOfInfection');

    $v->rule('required', 'lastContact');
    $v->rule('dateBefore', 'lastContact', $dateTomorrow);

    $v->rule('required', 'category');
    $v->rule('integer', 'category');
    $v->rule('min', 'category', 1);
    $v->rule('max', 'category', 3);

    $v->rule('required', 'reason');

    $v->rule('required', 'hasSymptoms');
    $v->rule('boolean', 'hasSymptoms');

    $v->rule(function($field, $value, $params, $fields) {
      return $this->isHasSymptomsSetValid($field, $value, $params, $fields);
    }, 'symptoms');

    $v->rule('dateBefore', 'beginOfSymptoms', $dateTomorrow);

    $v->rule(function($field, $value, $params, $fields) {
      return $this->isHasSymptomsSetValid($field, $value, $params, $fields);
    }, 'beginOfSymptoms');

    $v->rule('integer', 'severity');
    $v->rule('min', 'severity', 0);
    $v->rule('max', 'severity', 10);

    $v->rule(function($field, $value, $params, $fields) {
      return $this->isHasSymptomsSetValid($field, $value, $params, $fields);
    }, 'severity');

    $v->rule('required', 'preconditions');
    $v->rule('array', 'preconditions');
    $v->rule('subset', 'preconditions', ['chronic-heart-disease', 'lung-disease', 'chronic-liver-disease', 'diabetes-mellitus', 'cancer', 'weakened-immune-system']);

    $v->rule('required', 'terms');
    $v->rule('array', 'terms');

    $v->rule(function($field, $value, $params, $fields) {
      return is_array($value) && in_array('tos', $value) && in_array('privacy', $value);
    }, 'terms');

    if (!$v->validate()){
      $engine = $this->getEngine();

      $engine->json([
        'errorCode' => 'FIELDS_NOT_FILLED',
        'data' => array_keys($v->errors())
      ], 400);

      die();
    }
  }

  private function isHasSymptomsSetValid($field, $value, $params, $fields){
    if ((!isset($fields['hasSymptoms']) || $fields['hasSymptoms'] === false) && ($value === static::STRING_INVALID)){
      return true;
    }

    return isset($fields['hasSymptoms']) && ($fields['hasSymptoms'] === true) && ($value !== static::STRING_INVALID);
  }

  private function getLanguage(){
    $headers = getallheaders();
    $language = isset($headers['Accept-Language']) ? $headers['Accept-Language'] : null;

    $languagesValid = ['de-DE', 'en-EN', 'fr-FR', 'it-IT'];
    return in_array($language, $languagesValid) ? $language : array_shift($languagesValid);
  }

  private function createPdf(array $jsonData){
    $language = $this->getLanguage();
    $pdf = new Application\PdfTk\Pdf(__DIR__.'/../../files/questionnaire/'.$language.'/optimized.pdf');

    $dataPdf = $this->buildPdfData($jsonData);

    $response = $pdf
      ->fillForm($dataPdf, 'UTF-8', false)
      ->send('questionnaire.pdf')
    ;

    if (!$response){
      $engine = $this->getEngine();

      $engine->json([
        'errorCode' => 'UNDEFINED',
        'message' => $pdf->getError()
      ]);

      die();
    }
  }

  private function buildPdfData(array $dataJson){
    $data = [];

    $dateTimeToday = new \DateTime();
    $data['Datum'] = $dateTimeToday->format('d.m.Y');

    $data['Nachname'] = $dataJson['lastName'];

    $dateTimeBirthday = \DateTime::createFromFormat('Y-m-d', $dataJson['birthday']);
    $data['GebDatum'] = $dateTimeBirthday->format('d.m.Y');

    // https://stackoverflow.com/a/19521323/9394463
    $age = $dateTimeBirthday->diff($dateTimeToday)->y;
    $data['Alter'] = $age;

    $data['Vorname'] = $dataJson['firstName'];

    $gender = $this->buildPdfDataGender($dataJson);
    $data['m/w/d'] = $gender;

    $data['Adresse'] = $dataJson['address']['street'];
    $data['PLZ'] = $dataJson['address']['zipcode'];

    $phoneMail = $this->buildPdfDataPhoneMail($dataJson);
    $data['Tel  Mail'] = $phoneMail;

    $data['Bezirk'] = $dataJson['address']['city'];

    // kreisfreie Stadt = not filled for now
    // oder Landkreis = not filled for now

    $data['Name der Person der Lokalität des Risikogebietes'] = $dataJson['sourceOfInfection'];

    $dateTimeLastContact = \DateTime::createFromFormat('Y-m-d', $dataJson['lastContact']);
    $data['Kontaktdatum'] = $dateTimeLastContact->format('d.m.Y');

    $category = $this->buildPdfDataCategory($dataJson);
    $reason = $dataJson['reason'];

    if ($category === 1){
      $data['Kategorie 1'] = 'X';
      $data['Abstand oder Kontakt mit dessen Atemwegssekreten sowie Speichelkontakt 1'] = $reason;
    } else if ($category === 2){
      $data['Kategorie 2'] = 'X';
      $data['ohne Schutzausrüstung bei mehr als 2m Abstand 1'] = $reason;
    } else if ($category === 3){
      $data['Kategorie 3'] = 'X';
      $data['Med. Personal'] = $reason;
    }

    $hasSymptoms = $dataJson['hasSymptoms'] === true;

    if ($hasSymptoms){
      $data['beschreiben Sie diese und kreuzen den Schweregrad Ihrer Symptomatik an 0  10'] = 'On';

      $dateTimeBeginOfSymptoms = \DateTime::createFromFormat('Y-m-d', $dataJson['beginOfSymptoms']);
      $data['Datum Symptombeginn'] = $dateTimeBeginOfSymptoms->format('d.m.Y');

      $data['zB Husten Schnupfen Fieber Halsschmerzen 1'] = $dataJson['symptoms'];
      $data['Bitte Zahl'] = $dataJson['severity'];
    } else {
      $data['Nein'] = 'On';
    }

    $preconditions = $dataJson['preconditions'];

    if (in_array('chronic-heart-disease', $preconditions)){
      $data['chronische Herzerkrankung'] = 'On';
    }

    if (in_array('lung-disease', $preconditions)){
      $data['Lungenerkrankungen z B Asthma COPD chronische Bronchitis'] = 'On';
    }

    if (in_array('chronic-liver-disease', $preconditions)){
      $data['chronische Lebererkrankungen'] = 'On';
    }

    if (in_array('diabetes-mellitus', $preconditions)){
      $data['Diabetes mellitus Zuckerkrankheit'] = 'On';
    }

    if (in_array('cancer', $preconditions)){
      $data['TumorKrebserkrankungen'] = 'On';
    }

    if (in_array('weakened-immune-system', $preconditions)){
      $data['Patienten mit geschwächtem Immunsystem inkl HIVAIDS'] = 'On';
    }

    return $data;
  }

  private function buildPdfDataGender(array $dataJson){
    $dataJsonGender = $dataJson['gender'];

    if ($dataJsonGender === 'male'){
      return 'm';
    } else if ($dataJsonGender === 'female'){
      return 'w';
    } else {
      return 'd';
    }
  }

  private function buildPdfDataPhoneMail(array $dataJson){
    $dataJsonPhone = $dataJson['phone'];
    $dataJsonEmail = isset($dataJson['email']) ? $dataJson['email'] : null;

    $data = $dataJsonPhone;

    if ($dataJsonEmail){
      $data .= ' / ';
      $data .= $dataJsonEmail;
    }

    return $data;
  }

  private function buildPdfDataCategory(array $dataJson){
    return (int) $dataJson['category'];
  }

}