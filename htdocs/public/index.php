<?php

define('COMPOSER_PATH', __DIR__.'/../vendor/autoload.php');

setlocale(LC_ALL, 'en_US.UTF-8');
date_default_timezone_set('Europe/Berlin');

ini_set('log_errors', 1);
ini_set('error_log', __DIR__.'/../errorlogs/'.date('Y-m-d').'-php.txt');

require COMPOSER_PATH;

try{
  $dotenv = Dotenv\Dotenv::createImmutable(__DIR__.'/../config/');
  $dotenv->load();

  new Application\App();

// TODO: save errors somewhere
} catch (Throwable $e){
  echo $e->getMessage();
} catch (Exception $e){
  echo $e->getMessage();
}